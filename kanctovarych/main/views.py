from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, AllowAny
from .permissions import IsAdminOrReadOnly, IsOwnerOrReadOnly

from .models import Product, Comment

from .serializers import ProductSerializer, CommentSerializer

from .tasks import send_email

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (AllowAny,)

    @method_decorator(cache_page(15))
    @action(detail=True, methods=["get"])
    def comments(self, request, pk=None):
        product = self.get_object()
        serializer = CommentSerializer(product.comments.all(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["post"])
    def comment(self, request, pk=None):
        data = request.data.copy()
        data["product"] = pk
        serializer = CommentSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        send_email.delay(
            "Вы оставили комментарий",
            "Вы оставили комментарий",
            ["to@example.com"]
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated, )
    authentication_classes = (TokenAuthentication, )
    #permission_classes = (IsOwnerOrReadOnly, IsAdminOrReadOnly)
