from django.db import models

from django.contrib.auth.models import User


class Product(models.Model):
    title = models.CharField(max_length=255)
    img = models.CharField(max_length=255)
    desc = models.CharField(max_length=1000)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    url = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"


class Comment(models.Model):
    id = models.CharField(max_length=255,primary_key=True)
    title = models.CharField(max_length=255)
    text = models.CharField(max_length=1000)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="comments",
        related_query_name="comment",
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name="comments",
        related_query_name="comment",
    )
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"


class Log(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='logs',
    )
    date = models.DateTimeField()
    request_url = models.CharField(max_length=255)
    request_method = models.CharField(max_length=255)
    browser = models.CharField(max_length=255)
    os = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Лог"
        verbose_name_plural = "Логи"
