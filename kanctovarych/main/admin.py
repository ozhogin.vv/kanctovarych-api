from django.contrib import admin

from .models import Product, Comment, Log

from import_export.admin import ImportExportModelAdmin

from simple_history.admin import SimpleHistoryAdmin


class ProductAdmin(ImportExportModelAdmin, SimpleHistoryAdmin, admin.ModelAdmin):
    list_display = ('title', 'price')
    search_fields = ('title', 'desc')


class CommentAdmin(ImportExportModelAdmin, SimpleHistoryAdmin, admin.ModelAdmin):
    list_display = ('title', 'text')
    search_fields = ('title', 'text')


class LogAdmin(ImportExportModelAdmin, SimpleHistoryAdmin, admin.ModelAdmin):
    list_display = ('user', 'date', 'request_url', 'request_method')
    readonly_fields = ('user', 'date', 'request_url', 'request_method', 'browser', 'os')


admin.site.site_title = 'Kanctovarych'
admin.site.site_header = 'Kanctovarych'

admin.site.register(Product, ProductAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Log, LogAdmin)
