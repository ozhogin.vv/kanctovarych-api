from datetime import datetime

import httpagentparser
from django.http import HttpResponse

from main.models import Log


class LogMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        black_urlpatterns = [
            'admin/', 'token/', 'auth/'
        ]
        for url in black_urlpatterns:
            if request.path.find(url) != -1:
                return response
        user = request.user
        date = datetime.now()
        request_url = request.path
        request_method = request.method
        agent = request.META.get('HTTP_USER_AGENT')
        browser = httpagentparser.simple_detect(agent)[1]
        os = httpagentparser.simple_detect(agent)[0]
        if request.user.is_authenticated:
            Log.objects.create(
                user=user,
                date=date,
                request_url=request_url,
                request_method=request_method,
                browser=browser,
                os=os,
            )
        return response
