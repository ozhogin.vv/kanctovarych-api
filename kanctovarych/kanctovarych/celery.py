import os

from celery import Celery

from kanctovarych.schedule import CELERY_BEAT_SCHEDULE


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'kanctovarych.settings')

app = Celery('kanctovarych')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = CELERY_BEAT_SCHEDULE

app.autodiscover_tasks()


@app.task(bind=True, ignore_result=True)
def debug_task(self):
    print(f'Request: {self.request!r}')