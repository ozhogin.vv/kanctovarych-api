from celery.schedules import crontab


CELERY_BEAT_SCHEDULE = {
    'send_email_every_minute': {
        'task': 'main.tasks.send_email',
        'schedule': crontab(minute='*/1'),
        'args': ('beat', 'message from celery-beat', ['to@example.com'])
    }
}
